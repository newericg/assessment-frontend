import axios from "axios";

export default async function(id) {
  try {
    let res = await axios(`http://localhost:8888/api/V1/categories/${id}`)
    return res.data
  } catch (err) {
    console.log(err)
  }
}