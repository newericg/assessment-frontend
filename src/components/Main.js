import React, { Fragment, useState, useEffect } from 'react'
import ReactLoading from 'react-loading';
import queryString from 'query-string'
import Breadcrumb from './Breadcrumb';
import Sidebar from './Sidebar'


//import APIs
import categories_list from "../services/list";
import category from "../services/category";




//Styles
const mainStyle = {
  marginTop: '30px',
  color: 'red',
}

const btnBuy = {
  color: 'white',
  backgroundColor: '#7FBDB8'
}

const selectBar = {
  width: '50%',
  border: 'solid .6px grey',
  borderRadius: '4px',
  backgroundColor: 'white',
  padding: '5px 0'
}

const orderBar = {
  width: '50%', 
  display:'flex', 
  justifyContent:'flex-end', 
  alignItems:'center'
}

//FILTRO GERAL
function filterItems(items, query)  {
  return items.filter((item) => {
  if(Object.keys(query).length === 0){
    return true
  }
  let itemFilter = item.filter[0]
  let match = Object.keys(query).map((key) => {
    return (query[key] === itemFilter[key])
      }) 
        return !match.includes(false)
      })
  }

//Main function

function Main(props)  {
  const [categoryResponse, setCategoryResponse] = useState()
  const [items, setItems] = useState()
  const [loading, setLoading] = useState(false)
  const [listName, setListName] = useState()

  let match = props.match
  let location = props.location

//
  useEffect(() => {
    (async () => {
      let list = await categories_list()
      let category_id = list.filter((item) => item.path === match.params.name)[0].id
      let category_name = list.filter((item) => item.path === match.params.name)[0].name
      let response = await category(category_id)

      let category_items = filterItems(response.items, queryString.parse(props.location.search))

      setCategoryResponse(Object.assign(response, {items: category_items}))
      setItems(category_items)
      setLoading(true)
      setListName(category_name)
    })();
  }, [match.params.name, props]);

  //Loading enquanto a chamada não é completada
  if (loading === false) {
    return (
      <div className="content_center" style={{width:'80%'}}>
        <ReactLoading type={'spin'} color={'red'} height={67} width={75} />
      </div>
    )
  //Pagina principal
  } else {
    return (
      <Fragment>
        <Breadcrumb currentPage = {listName}/>

        <Sidebar categoryResponse={categoryResponse} location={location}/>

        <div className="main_content" style={mainStyle}>
          <div className="row_class">
            <div style={{width:'100%'}}>
              <h2 style={{ fontWeight: '400' }}>
                {listName}
              </h2>
            </div>
            <div style={{width:'100%'}}>
              <hr/>
              <div className="row_class">
                <div style={{color: 'gray', width:'50%'}}>
                  <i className="material-icons">view_module</i> 
                  <i className="material-icons">view_list</i> 
                </div>
                <div style={orderBar}>
                  <span style={{color: 'gray', paddingRight:'20px'}}>ORDENAR POR</span>
                    <select id='form' style={selectBar}>
                      <option value="price">Preço</option>
                      <option value="name">Nome</option>
                    </select>
                </div>
              </div>
              <hr/>
            </div>
            <div style={{width:'100%'}}>
              <div className="row_center">
                {items.map((item, i) => (
                  <div className="items_card_class" key={i}>
                    <img className="img_products" src={`../${item.image}`} alt={item.name} />
                    <p style={{ color: 'gray', textAlign:'center'}}>{item.name}</p>
                    <p style={{textAlign:'center'}}> <b>R$ {item.price}</b></p>
                    <button style={btnBuy} type="button" className="defaltu_btn">Comprar</button>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>

      </Fragment>
    )

  }
}
export default Main