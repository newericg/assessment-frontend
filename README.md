
# Tecnologias Utilizadas
- React.
- SCSS como pré processador CSS
- API consumida através do axios.
-  CORS para habilitar tráfego entre origens diferentes.
(Se o navegador bloquear, será necessário instalação de plugins: Firefox - https://addons.mozilla.org/pt-BR/firefox/addon/cors-everywhere/ | Chrome: https://chrome.google.com/webstore/detail/allow-cors-access-control/lhobafahddgcelffkeicbaginigeejlf)

## Como iniciar o projeto

- Instale as dependências
```
npm install
```
- Rode a API
```
npm run api
```
- Rode a aplicação
```
npm start
```