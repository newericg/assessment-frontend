import React from 'react';
import './App.scss';
import Main from './components/Main';
import { Switch, Route } from 'react-router-dom'


function App() {
  return (
    <Switch>
      <Route path="/categories/:name" exact={true} component={Main} />
    </Switch>
  );
}

export default App;