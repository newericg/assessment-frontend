import React, {Fragment, useState, useEffect} from 'react'
import Logo from '../assets/LOGOWEB.png'
import { Link } from 'react-router-dom'

import categories_list from "../services/list";

const headerStyle = {
  margin: '0',
  width: '100%',
  height: '6vh',
  backgroundColor: 'black',
}

const rowWhiteStyle = {
  margin: '0',
  width: '100%',
  height: '16vh',
  backgroundColor: 'white'
}

const inputStyle = {
  width: '100%',
  height: '100%',
}

const inputStyleMobile = {
  height: '15%',
  fontSize: '1.5rem',
  width: '100%'
}

const buttonStyleSearch = {
  width: '100%',
  borderRadius: '0px',
  fontWeight: 'bold',
  fontFamily: 'Open Sans - Extrabold',
  textTransform: 'uppercase',
  backgroundColor: 'red',
  color: 'white'
}

const redNavStyle = {
  margin: '0',
  width: '100%',
  height: '7vh',
  backgroundColor: 'red',
  color: 'white',
  fontWeight: 'bold',
  textTransform: 'uppercase',
  fontSize:'1.5rem'
}

const iconStyle ={
  fontSize: '48px',
  color: 'red'
}

const hambStyle = {
  fontSize: '48px',
}

const hambMenu = {
  width: '100%', 
  height:'80vh', 
  backgroundColor: 'white', 
  position: 'absolute',
}

const darkHeader = {
  color: 'white', 
  margin:'0'
}

const btnStyle = {
  display: 'inline-block',
	fontWeight: '400',
	textAlign: 'center',
	verticalAlign: 'middle',
	cursor: 'pointer',
	userSelect: 'none',
	backgroundColor: 'transparent',
	border: '1px solid transparent',
	padding: '.375rem .75rem',
	fontSize: '1rem',
	lineHeight: '1.5',
	borderRadius: '.25rem',
}


function Header(){

  const [lists, setLists] = useState()
  const [loading, setLoading] = useState(false)
  let [hamb, setHamb] = useState(false)
  let [search, setSearch] = useState(false)


  useEffect(() => {

    (async() => {
     let list = await categories_list()
     setLists(list)
     setLoading(true)
    })();

  }, []);

  function openMenu(e) {
    e.preventDefault();
    setHamb(hamb = !hamb)
  }

  function openSearch(e) {
    e.preventDefault();
    setSearch(search = !search)
  }


  if(loading === false){
    return (
      <h1>LOADING</h1>
    )
  } else {
  return(
    <Fragment>

      {/* // TOP BLACK HEADER */}

      <div className="row_center" style={headerStyle}>
        <div className="dark_header_class">
          <p style={darkHeader}>
            <u> <b>Acesse sua conta</b></u> ou  <u> <b>Cadastre-se</b></u>
          </p>
        </div>

    {/* // WHITE HEADER WITH LOGO */}

      </div>
      <div className="row_center" style={rowWhiteStyle}>

        <div className="white_header_logo">

        {/* // MOBILE MENU */}
          <button onClick={openMenu} style={btnStyle}>  
            <i style={hambStyle} className="header_btns material-icons">menu</i>  
          </button>

        {/* // WEBJUMP LOGO */}
          <img src={Logo} alt="Logowebjump" style={{width: '200px'}}/>

        {/* // MOBILE SEARCH */}
          <button onClick={openSearch} style={btnStyle}>  
            <i style={iconStyle} className="header_btns material-icons">search</i> 
          </button>

        </div>

       {/* SEARCH MENU FOR SMALL SCREEN */}
        <div style={{display: search ? 'flex' : 'none', height:'30vh', width: '100%', padding: '0', margin: '15px 0'}}>
          <div className="content_center" style={{width: '100%', margin: '0', position: 'absolute', zIndex: '2' }}>
              <div style={{padding: '0', width:'40%'}}>
                <input style={inputStyleMobile} type="text"/>
              </div>
              <div style={{padding: '0', width:'40%'}}>
                <button className="defaltu_btn" type="button" style={buttonStyleSearch}>Search</button>
              </div>
            </div>
        </div>

        {/* MENU FOR SMALL SCREEN */}
        <div style={{display: hamb ? 'flex' : 'none', height:'100vh', width: '100%', margin: '15px 0'}}>
          <div  style={{width: '100%', position: 'absolute', zIndex: '2'}}>
              <div  style={hambMenu}>
                {lists.map((list, i) => (
                <div key={i} onClick={openMenu} className="content_center" style={{height: '20%'}}>
                  <Link style={{color: 'red', fontSize: '2rem'}} to={`/categories/${list.path}`} >{list.name}</Link>
                </div>
                ))}
              </div>
          </div>
        </div>

        {/* // SEARCHBAR MEDIUM SCREEN */}
        <div className="search_bar_class">
          <div className="row_class" style={{width: '100%'}}>
            <div style={{padding:'0', width:'80%'}}>
              <input type="text" style={inputStyle}/>
            </div>
            <div style={{padding:'0', width:'20%'}}>
              <button type="button" className="defaltu_btn"  style={buttonStyleSearch}>Search</button>
            </div>
          </div>
        </div>
      </div>

      {/* // RED NAVBAR */}
      <div className="row_class red_header" style={redNavStyle}>
        <div style={{width: '15%'}}>
          <Link to="/" >Pagina Inicial</Link>
        </div>
          {lists.map((list, i) => (
            <div key={i} style={{width: '15%'}}>
              <Link to={`/categories/${list.path}`} >{list.name}</Link>
            </div>
          ))}
        <div style={{width: '15%'}}>
          <Link to='#'>Contato</Link>
        </div>
      </div>
    </Fragment>
  )
}
}

export default Header