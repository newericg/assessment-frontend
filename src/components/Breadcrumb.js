import React from 'react'

function Breadcrumb(props){
  return(
    <div className="breadcrumb_class">
      <p style={{margin: '0'}}>
        Pagina Inicial  &gt; <span style={{color: 'red', fontSize: '1.2rem'}}>{props.currentPage}</span> 
      </p>
    </div>
  )
}

export default Breadcrumb