import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { BrowserRouter } from 'react-router-dom'
import Header from './components/Header';
import Footer from './components/Footer'

ReactDOM.render(
    <BrowserRouter>
			<div className="container_class">
				<Header />
				<div className="row_center">
					<div className="main_page_width">
						<div className="row_center">
							<App />
						</div>
					</div>
				</div>
				<Footer />
			</div>
    </ BrowserRouter>
    , document.getElementById('root'));
