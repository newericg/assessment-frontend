import React, {Fragment, useState, useEffect} from 'react'
import ReactLoading from 'react-loading';
import { Link } from 'react-router-dom'
import queryString from 'query-string'

const sideBarDiv = {
  border: '1px solid #E9E8E6',
  color: 'red',
  overflow: 'hidden',
  height: 'auto'
}

function FilterOptions (apiResponse) {

  let renderObject = {}
  apiResponse.items.forEach((item) => {
    item.filter.forEach((filter) => {
      let currentKey = Object.keys(filter)[0]
      //Criar a chave se não existe
      if (!renderObject[currentKey]) {
        renderObject[currentKey] = {options: []}
      }
      //Guarda a tradução se ela estiver presente
      renderObject[currentKey].translation = apiResponse.filters[0][currentKey]

      //Guarda as opções dessa chave em um array
      if (!renderObject[currentKey].options.includes(filter[currentKey])) 
      { renderObject[currentKey].options.push(filter[currentKey]) }
    })
  })
  return renderObject
}

function mergeSeachParams(newOption, location) {
  let currentQuery = queryString.parse(location.search);
  let newQuery = Object.assign(currentQuery, newOption)
  return queryString.stringify(newQuery);
}

function Sidebar({ categoryResponse, location }){
  
  const [filters, setFilters] = useState()
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    // let result = FilterOptions(categoryResponse)
    setLoading(true)
    setFilters(FilterOptions(categoryResponse))
  }, [categoryResponse])
  

  if (loading === false) {
    return (
      <div className="content_center" style={{width:'75%', marginTop:'50px'}}>
        <ReactLoading type={'spin'} color={'red'} height={67} width={75} />
      </div>
    )
  //Pagina principal
  } else {

  return(
    <Fragment>
        <div className="sidebar_class" style={sideBarDiv}>
          <h3 style={{ fontWeight: 'bolder' }}>FILTRE POR</h3>

          <h5 style={{ fontWeight: 'bolder', color: '#7FBDB8', paddingTop: '30px' }}>CATEGORIAS</h5>
          <ul style={{paddingLeft:'20px'}}>
            <li style={{ color: 'gray' }}>Roupas</li>
            <li style={{ color: 'gray' }}>Sapatos</li>
            <li style={{ color: 'gray' }}>Acessórios</li>
          </ul>
          

          
          
            {
              Object.keys(filters).map((key, i) => {
                return [
                  <h5 key={i} style={{ fontWeight: 'bolder', color: '#7FBDB8',textTransform: 'uppercase', paddingTop:'20px' }}>
                    {filters[key].translation || key}
                  </h5>,
                  <ul style={{paddingLeft:'20px'}}>
                    {
                      filters[key].options.map((option, i) => {
                        return <li style={{ color: 'gray' }} key={i}>
                            <Link 
                              style={{ color: 'gray', fontSize:'1.2rem' }} 
                              to={`?${mergeSeachParams({[key]: option}, location)}`} 
                            >
                              {option}
                            </Link>
                        </li>
                      })
                    }
                  </ul>
                ]
              })
            }
          

        </div>
    </Fragment>
  )
}}

export default Sidebar