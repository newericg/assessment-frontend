import axios from "axios";

export default async function() {
  try {
    let res = await axios('http://localhost:8888/api/V1/categories/list')
    let response  =  res.data
    return response.items
  } catch (err) {
    console.log(err)
  }
}  